package pl.jaskolek.glueblocks;

import pl.jaskolek.glueblocks.model.Context;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.scenes.scene2d.Stage;

public abstract class BaseScreen implements Screen, ActionProviderInterface {

	protected Stage _stage;
	protected ActionListener _actionListener;
	protected Context _context;

	public static final float STAGE_WIDTH = 100;
	public static final float STAGE_HEIGHT = 100;

	public BaseScreen(Context context) {
		super();
		_context = context;
		_stage = new Stage(STAGE_WIDTH, STAGE_HEIGHT);
	}

	@Override
	public void render(float delta) {
		Gdx.gl.glClear(GL10.GL_COLOR_BUFFER_BIT);
		_stage.act(Gdx.graphics.getDeltaTime());

		_stage.draw();
	}

	@Override
	public void resize(int width, int height) {
		// _stage.setViewport(STAGE_WIDTH, STAGE_HEIGHT, true);
		_stage.getCamera().translate(_stage.getGutterWidth(),
				_stage.getGutterHeight(), 0);
	}

	@Override
	public void show() {
		Gdx.input.setInputProcessor(_stage);
	}

	/**
	 * _context getter
	 * 
	 * @return
	 */
	public Context getContext() {
		return _context;
	}

	/**
	 * context setter
	 * 
	 * @param context
	 */
	public void setContext(Context context) {
		_context = context;
	}

	public void setActionListener(ActionListener listener) {
		_actionListener = listener;
	}

	public ActionListener getActionListener() {
		return _actionListener;
	}

}
