package pl.jaskolek.glueblocks;

import pl.jaskolek.glueblocks.model.Board;
import pl.jaskolek.glueblocks.model.BoardData;
import pl.jaskolek.glueblocks.model.Context;
import pl.jaskolek.glueblocks.screens.EditorScreen;
import pl.jaskolek.glueblocks.screens.GameScreen;
import pl.jaskolek.glueblocks.screens.MenuScreen;

import com.badlogic.gdx.Game;

public class GlueBlocks extends Game implements ActionListener{

	private Context _context;
	private MenuScreen _menuScreen;
	private GameScreen _gameScreen;
	
	private BoardFactory _boardFactory;
	
	@Override
	public void create() {
		_context = loadGameState();
		
		_menuScreen = new MenuScreen(_context);
		_menuScreen.setActionListener(this);
//		_gameScreen = new GameScreen(_context);
		_gameScreen = new EditorScreen(_context);
		_gameScreen.setActionListener(this);
		
		_boardFactory = new BoardFactory();
		setScreen(_menuScreen);
	}

	@Override
	public void pause() {
		saveGameState(_context);
		super.pause();
	}

	public Context loadGameState() {
		return new Context();
	}

	public void saveGameState(Context context) {
		// TODO Auto-generated method stub

	}

	@Override
	public void playBoard(BoardData boardData) {
		// TODO Auto-generated method stub
		
		Board board = _boardFactory.fromFile( boardData.filePath );
		_gameScreen.setBoard(board);
		setScreen(_gameScreen);
	}

}
