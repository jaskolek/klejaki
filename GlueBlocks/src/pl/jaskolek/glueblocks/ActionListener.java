package pl.jaskolek.glueblocks;

import pl.jaskolek.glueblocks.model.BoardData;

public interface ActionListener {

	public void playBoard( BoardData boardData );
}
