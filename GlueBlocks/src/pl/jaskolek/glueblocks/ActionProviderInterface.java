package pl.jaskolek.glueblocks;

public interface ActionProviderInterface {
	public void setActionListener( ActionListener listener );
	public ActionListener getActionListener();
}
