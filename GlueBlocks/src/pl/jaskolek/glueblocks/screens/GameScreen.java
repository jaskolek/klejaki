package pl.jaskolek.glueblocks.screens;

import pl.jaskolek.glueblocks.BaseScreen;
import pl.jaskolek.glueblocks.model.Board;
import pl.jaskolek.glueblocks.model.Context;
import pl.jaskolek.glueblocks.screens.game.BlockActor;
import pl.jaskolek.glueblocks.screens.game.BoardActor;
import pl.jaskolek.glueblocks.screens.game.FieldActor;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;

public class GameScreen extends BaseScreen {

	protected BoardActor _boardActor = null;
	protected Board _board = null;
	
	public GameScreen(Context context) {
		super(context);
	}

	/**
	 * creating new Board actor with board
	 * 
	 * @param board
	 */
	public void setBoard(Board board) {
		if (_boardActor != null) {
			_stage.getRoot().removeActor(_boardActor);
		}
		_board = board;
		_boardActor = new BoardActor(board);
		_boardActor.setSize(STAGE_WIDTH, STAGE_HEIGHT);
		_stage.addActor(_boardActor);
		_stage.setKeyboardFocus(_boardActor);
		
		_boardActor.addListener( new InputListener(){
			@Override
			public boolean touchDown(InputEvent event, float x, float y,
					int pointer, int button) {
				Actor target = event.getTarget();
				
				if( target instanceof FieldActor ){
					
				}
				if( target instanceof BlockActor ){
					((BlockActor) target).setIsSelected(true);
				}
				return super.touchDown(event, x, y, pointer, button);
			}
		});
	}

	public Board getBoard(){
		return _board;
	}
	
	@Override
	public void render(float delta) {
		Gdx.gl.glClear(GL10.GL_COLOR_BUFFER_BIT);
		_stage.act(Gdx.graphics.getDeltaTime());

		_stage.draw();
	}

	@Override
	public void resize(int width, int height) {
//		_stage.setViewport(STAGE_WIDTH, STAGE_HEIGHT, true);
		_stage.getCamera().translate(_stage.getGutterWidth(),
				_stage.getGutterHeight(), 0);
	}

	@Override
	public void hide() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void dispose() {
		// TODO Auto-generated method stub
		
	}

}
