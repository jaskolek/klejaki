package pl.jaskolek.glueblocks.screens.game;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.badlogic.gdx.scenes.scene2d.Group;

import pl.jaskolek.glueblocks.model.Field;

public class FieldActor extends Group {

	private ShapeRenderer _shapeRenderer;
	private Field _field;

	public Field getField() {
		return _field;
	}

	public FieldActor( Field field ) {
		super();
		_field = field;
		_shapeRenderer = new ShapeRenderer();
	}

	@Override
	public void draw(SpriteBatch batch, float parentAlpha) {

		batch.end();

		_shapeRenderer.setProjectionMatrix(batch.getProjectionMatrix());
		_shapeRenderer.setTransformMatrix(batch.getTransformMatrix());
		_shapeRenderer.translate(getX(), getY(), 0);
		
		_shapeRenderer.begin(ShapeType.Filled);

		Color color = null;
		switch( _field.type ){
		case END_GROUND:
			color = new Color(1f,0.8f,0.8f,1f);
			break;
		case NORMAL_GROUND:
			color = new Color(0.8f,0.8f,0.8f,1f);
			break;
		case OUTSIDE:
			color = new Color(0f,0f,0f,1f);
			break;
		case WALL:
			color = new Color(0.4f,0.4f,0.4f,1f);
			break;
		}
		
		_shapeRenderer.setColor(color);

		_shapeRenderer.rect(0, 0, getWidth(), getHeight());
		_shapeRenderer.end();

		batch.begin();
		super.draw(batch, parentAlpha);
	}
}
