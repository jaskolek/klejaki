package pl.jaskolek.glueblocks.screens.game;

import pl.jaskolek.glueblocks.model.Block;
import pl.jaskolek.glueblocks.model.Board;
import pl.jaskolek.glueblocks.model.Field;

import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.utils.Array;

public class BoardActor extends Group {

	private Board _board;
	private Array<FieldActor> _fieldActors;
	private Array<BlockActor> _blockActors;

	private float _fieldWidth = 0;
	private float _fieldHeight = 0;

	public BoardActor(Board board) {
		super();
		_board = board;
		_blockActors = new Array<BlockActor>();

		_fieldActors = new Array<FieldActor>(_board.getRowsCount());
		for (int row = 0; row < _board.getRowsCount(); ++row) {
			for (int col = 0; col < _board.getColsCount(); ++col) {
				Field field = _board.getField(row, col);

				FieldActor fieldActor = new FieldActor(field);
				_fieldActors.add(fieldActor);
				addActor(fieldActor);
			}
		}
	}

	@Override
	protected void sizeChanged() {
		super.sizeChanged();

		_fieldHeight = getHeight() / _board.getRowsCount();
		_fieldWidth = getWidth() / _board.getColsCount();

		for (FieldActor fieldActor : _fieldActors) {
			Field field = fieldActor.getField();
			float x = field.col * _fieldWidth;
			float y = field.row* _fieldHeight;

			fieldActor.setPosition(x, y);
			fieldActor.setSize(_fieldWidth, _fieldHeight);
		}

		for (BlockActor blockActor : _blockActors) {
			blockActor.setBlockSize(_fieldWidth, _fieldHeight);
		}
	}

	/**
	 * removes all empty block actors
	 */
	public void removeEmptyBlockActors(){
		for( BlockActor blockActor: _blockActors ){
			if( blockActor.getBlock().isEmpty() ){
				_blockActors.removeValue(blockActor, true);
			}
		}
	}
	
	/**
	 * removes and creates new block actors
	 */
	public void updateBlockActors(){
		//remove all block actors
		for( BlockActor blockActor: _blockActors ){
			removeActor(blockActor);
		}
		_blockActors.clear();
		
		//add new block actors
		for(Block block: _board.getBlocks()){
			BlockActor blockActor = new BlockActor(block);
			addBlockActor(blockActor);
		}
	}
	
	/**
	 * adds block actor on board
	 * 
	 * @param blockActor
	 */
	public void addBlockActor(BlockActor blockActor) {
		_blockActors.add(blockActor);
		blockActor.setBlockSize(_fieldWidth, _fieldHeight);
		addActor(blockActor);
	}

	/**
	 * removes block actor from board
	 * 
	 * @param blockActor
	 */
	public void removeBlockActor(BlockActor blockActor) {
		_blockActors.removeValue(blockActor, false);
		removeActor(blockActor);
	}

	public Board getBoard() {
		return _board;
	}

	public void setBoard(Board board) {
		_board = board;
	}

}
