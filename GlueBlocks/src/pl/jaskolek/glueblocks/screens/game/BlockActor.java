package pl.jaskolek.glueblocks.screens.game;

import pl.jaskolek.glueblocks.model.Block;
import pl.jaskolek.glueblocks.model.BlockPiece;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.badlogic.gdx.scenes.scene2d.Actor;

public class BlockActor extends Actor{

	protected ShapeRenderer _sr;
	
	protected Block _block;
	protected float _blockWidth;
	protected float _blockHeight;
	protected boolean _isSelected = false;
	
	public BlockActor( Block block ) {
		_block = block;
		_sr = new ShapeRenderer();
	}

	@Override
	public Actor hit(float x, float y, boolean touchable) {
		int row = (int) (y / _blockHeight);
		int col = (int) (x / _blockWidth);
		
		
		for( BlockPiece piece: _block.pieces ){
			if( piece.row == row && piece.col == col ){
				return this;
			}
		}
		return null;
	}
	
	@Override
	public void draw(SpriteBatch batch, float parentAlpha) {
		batch.end();
		
		_sr.setProjectionMatrix( batch.getProjectionMatrix() ); 
		_sr.setTransformMatrix( batch.getTransformMatrix() );
		
		
		_sr.begin(ShapeType.Filled);
		if( _isSelected ){
			_sr.setColor(Color.BLUE);
		}else{
			_sr.setColor(Color.GREEN);
		}
		
		for( BlockPiece piece: _block.pieces ){
			float x = piece.col * _blockWidth;
			float y = piece.row * _blockHeight;
			
			_sr.rect(x, y, _blockWidth, _blockHeight);
		}
		
		_sr.end();
		
		batch.begin();
		super.draw(batch, parentAlpha);
	}

	
	
	
	public void setBlockSize( float blockWidth, float blockHeight ){
		_blockHeight = blockHeight;
		_blockWidth = blockWidth;
	}
	
	public float getBlockWidth() {
		return _blockWidth;
	}

	public void setBlockWidth(float blockWidth) {
		_blockWidth = blockWidth;
	}

	public float getBlockHeight() {
		return _blockHeight;
	}

	public void setBlockHeight(float blockHeight) {
		_blockHeight = blockHeight;
	}

	public boolean isSelected() {
		return _isSelected;
	}

	public void setIsSelected(boolean isSelected) {
		_isSelected = isSelected;
	}

	public Block getBlock() {
		return _block;
	}
}
