package pl.jaskolek.glueblocks.screens.menu;

import pl.jaskolek.glueblocks.model.BoardData;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.badlogic.gdx.scenes.scene2d.Actor;

public class BoardStateActor extends Actor {

	private ShapeRenderer _shapeRenderer;
	private BoardData _boardData;

	public BoardStateActor(BoardData boardData) {
		super();
		_shapeRenderer = new ShapeRenderer();
		_boardData = boardData;
	}

	@Override
	public void draw(SpriteBatch batch, float parentAlpha) {

		batch.end();

		_shapeRenderer.setProjectionMatrix(batch.getProjectionMatrix());
		_shapeRenderer.setTransformMatrix(batch.getTransformMatrix());

		_shapeRenderer.translate(getX(), getY(), 0);

		_shapeRenderer.begin(ShapeType.Filled);
		_shapeRenderer.setColor(Color.WHITE);

		_shapeRenderer.rect(0, 0, getWidth(), getHeight());

		_shapeRenderer.end();
		batch.begin();
		super.draw(batch, parentAlpha);
	}

	public BoardData getBoardData() {
		return _boardData;
	}

}
