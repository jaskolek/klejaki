package pl.jaskolek.glueblocks.screens.menu;

import pl.jaskolek.glueblocks.model.BoardData;

import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.utils.Array;

public class MenuTableActor extends Group {

	private Array<Array<BoardStateActor>> _boardStateActors;

	private float _fieldWidth = 0;
	private float _fieldHeight = 0;

	public static final int ROWS = 4;
	public static final int COLS = 4;

	public MenuTableActor(Array<BoardData> boardStates) {
		super();
		_boardStateActors = new Array<Array<BoardStateActor>>(ROWS);
		for (int row = 0; row < ROWS; ++row) {
			_boardStateActors.add(new Array<BoardStateActor>(COLS));
			for (int col = 0; col < COLS; ++col) {
				int index = row * COLS + col;
				if (boardStates.size > index) {
					BoardData boardState = boardStates.get(index);
					BoardStateActor boardStateActor = new BoardStateActor(
							boardState);

					_boardStateActors.get(row).add(boardStateActor);
					addActor(boardStateActor);
				} else {
					_boardStateActors.get(row).add(null);
				}
			}
		}
	}

	/**
	 * if size changes, resize all boardStateActors
	 */
	@Override
	protected void sizeChanged() {
		super.sizeChanged();

		_fieldWidth = getWidth() / COLS;
		_fieldHeight = getHeight() / ROWS;
		
		for (int row = 0; row < ROWS; ++row) {
			for (int col = 0; col < COLS; ++col) {
				BoardStateActor boardStateActor = _boardStateActors.get(row)
						.get(col);
				if (boardStateActor != null) {
					float x = col * _fieldWidth;
					float y = row* _fieldHeight;
					
					boardStateActor.setSize(_fieldWidth, _fieldHeight);
					boardStateActor.setPosition(x, y);
				}
			}
		}
	}

}
