package pl.jaskolek.glueblocks.screens;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;

import pl.jaskolek.glueblocks.BaseScreen;
import pl.jaskolek.glueblocks.model.BoardData;
import pl.jaskolek.glueblocks.model.Context;
import pl.jaskolek.glueblocks.screens.menu.BoardStateActor;
import pl.jaskolek.glueblocks.screens.menu.MenuTableActor;

public class MenuScreen extends BaseScreen{

	private MenuTableActor _menuTableActor;
	
	public MenuScreen(Context context) {
		super(context);
		_menuTableActor = new MenuTableActor( context.boardStates );
		_menuTableActor.setSize(STAGE_WIDTH, STAGE_WIDTH);
		
		_stage.addActor(_menuTableActor);
		
		_menuTableActor.addListener( new InputListener(){
			@Override
			public boolean touchDown(InputEvent event, float x, float y,
					int pointer, int button) {
				Actor target = event.getTarget();
				
				if( target instanceof BoardStateActor ){
					BoardStateActor boardStateActor = (BoardStateActor) target;
					BoardData boardData = boardStateActor.getBoardData();
					_actionListener.playBoard( boardData );
				}

				return super.touchDown(event, x, y, pointer, button);
			}
		});
	}

	@Override
	public void hide() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void dispose() {
		// TODO Auto-generated method stub
		
	}

	
}
