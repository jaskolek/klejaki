package pl.jaskolek.glueblocks.screens;

import pl.jaskolek.glueblocks.model.Block;
import pl.jaskolek.glueblocks.model.BlockPiece;
import pl.jaskolek.glueblocks.model.Context;
import pl.jaskolek.glueblocks.model.Field;
import pl.jaskolek.glueblocks.model.Field.Type;
import pl.jaskolek.glueblocks.screens.game.BlockActor;
import pl.jaskolek.glueblocks.screens.game.FieldActor;

import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.utils.IntArray;

public class EditorScreen extends GameScreen {

	private IntArray _pressedKeys = new IntArray();

	public EditorScreen(Context context) {
		super(context);

		_stage.addListener(new InputListener() {
			@Override
			public boolean keyDown(InputEvent event, int keycode) {
				_pressedKeys.add(keycode);

				return super.keyDown(event, keycode);
			}

			@Override
			public boolean keyUp(InputEvent event, int keycode) {
				_pressedKeys.removeValue(keycode);

				return super.keyUp(event, keycode);
			}

			@Override
			public boolean touchDown(InputEvent event, float x, float y,
					int pointer, int button) {
				Actor target = event.getTarget();
				
				
				if (target instanceof FieldActor) {
					FieldActor fieldActor = (FieldActor) target;
					Field field = fieldActor.getField();

					// tutaj wykonujemy rozne akcje edycji
					if (_pressedKeys.contains(Keys.I)) {
						field.type = Type.END_GROUND;
					}
					if (_pressedKeys.contains(Keys.O)) {
						field.type = Type.NORMAL_GROUND;
					}
					if (_pressedKeys.contains(Keys.K)) {
						field.type = Type.OUTSIDE;
					}
					if (_pressedKeys.contains(Keys.L)) {
						field.type = Type.WALL;
					}
					

					if (_pressedKeys.contains(Keys.D)) {
						Block block = _board.getBlock(field);
						if (block != null) {
							block.removePiece(field);

							_board.updateBlocks();
							_boardActor.updateBlockActors();
						}
					}
					if (_pressedKeys.contains(Keys.A)) {

						if (field.type == Type.NORMAL_GROUND
								|| field.type == Type.END_GROUND) {
							Block block = new Block();
							BlockPiece piece = new BlockPiece(field.row,
									field.col);

							block.pieces.add(piece);
							_board.addBlock(block);
							_board.updateBlocks(block);

							BlockActor blockActor = new BlockActor(block);
							_boardActor.addBlockActor(blockActor);
							_boardActor.removeEmptyBlockActors();
						}
					}
				}

				return super.touchDown(event, x, y, pointer, button);
			}
		});
	}

}
