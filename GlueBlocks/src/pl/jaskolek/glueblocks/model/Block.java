package pl.jaskolek.glueblocks.model;

import com.badlogic.gdx.utils.Array;

public class Block {

	public Array<BlockPiece> pieces = new Array<BlockPiece>();

	/**
	 * 
	 * @param field
	 * @return
	 */
	public boolean isOnField(Field field) {
		return isOnField(field.row, field.col);
	}

	/**
	 * checks if block lies on field (row,col)
	 * 
	 * @param row
	 * @param col
	 * @return
	 */
	public boolean isOnField(int row, int col) {
		for (BlockPiece piece : pieces) {
			if (piece.row == row && piece.col == col) {
				return true;
			}
		}
		return false;
	}

	/**
	 * checks if block is empty
	 * 
	 * @return
	 */
	public boolean isEmpty() {
		return pieces.size == 0;
	}

	/**
	 * returns block piece on position field
	 * 
	 * @param field
	 * @return
	 */
	public BlockPiece getPiece(Field field) {
		return getPiece(field.row, field.col);
	}

	/**
	 * returns block piece with position row,col
	 * 
	 * @param row
	 * @param col
	 * @return
	 */
	public BlockPiece getPiece(int row, int col) {
		for (BlockPiece piece : pieces) {
			if (piece.row == row && piece.col == col) {
				return piece;
			}
		}
		return null;
	}

	/**
	 * 
	 * @param field
	 */
	public void removePiece( Field field ){
		removePiece(field.row, field.col);
	}
	
	/**
	 * 
	 * @param row
	 * @param col
	 */
	public void removePiece( int row, int col ){
		BlockPiece piece = getPiece(row, col);
		if( piece != null ){
			removePiece( piece );
		}
	}
	
	/**
	 * removes piece from block
	 * @param blockPiece
	 */
	public void removePiece( BlockPiece blockPiece ){
		pieces.removeValue(blockPiece, true);
	}
	
}
