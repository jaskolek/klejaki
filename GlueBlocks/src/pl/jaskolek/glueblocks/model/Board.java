package pl.jaskolek.glueblocks.model;

import com.badlogic.gdx.utils.Array;

public class Board {

	private int _rows;
	private int _cols;

	private Array<Array<Field>> _fields = new Array<Array<Field>>();
	private Array<Block> _blocks = new Array<Block>();

	public Board() {
		this(0, 0);
	}

	public Board(int rows, int cols) {
		super();

		for (int row = 0; row < rows; ++row) {
			addRow();
		}
		for (int col = 0; col < cols; ++col) {
			addCol();
		}
	}

	public void addRow() {
		_rows++;
		_fields.add(new Array<Field>());
		for (int col = 0; col < _cols; ++col) {
			_fields.get(_rows - 1).add(new Field(_rows - 1, col));
		}
	}

	public void removeRow() {
		_rows--;
		_fields.pop();
	}

	public void addCol() {
		_cols++;
		for (int row = 0; row < _rows; ++row) {
			_fields.get(row).add(new Field(row, _cols - 1));
		}
	}

	public void removeCol() {
		_cols--;
		for (int row = 0; row < _rows; ++row) {
			_fields.get(row).pop();
		}
	}

	public int getRowsCount() {
		return _rows;
	}

	public int getColsCount() {
		return _cols;
	}

	/**
	 * checks if row and col are in board bounds
	 * 
	 * @param row
	 * @param col
	 * @return
	 */
	public boolean isInBounds(int row, int col) {
		return (row >= 0 && row < this.getRowsCount() && col >= 0 && col < this
				.getColsCount());
	}

	public boolean hasBlockOnField(Field field) {
		return hasBlockOnField(field.row, field.col);
	}

	/**
	 * checks if block is on field (row, col)
	 * 
	 * @param row
	 * @param col
	 * @return
	 */
	public boolean hasBlockOnField(int row, int col) {
		for (Block block : _blocks) {
			if (block.isOnField(row, col)) {
				return true;
			}
		}
		return false;
	}

	public Block getBlock(Field field) {
		return getBlock(field.row, field.col);
	}

	/**
	 * returns block from field row,col
	 * 
	 * @param row
	 * @param col
	 * @return
	 */
	public Block getBlock(int row, int col) {
		for (Block block : _blocks) {
			if (block.isOnField(row, col)) {
				return block;
			}
		}
		return null;
	}

	/**
	 * gets field (row,col)
	 * 
	 * @param row
	 * @param col
	 * @return
	 */
	public Field getField(int row, int col) {
		return _fields.get(row).get(col);
	}

	// remove empty blocks
	public void removeEmptyBlocks() {
		for (Block block : _blocks) {
			if (block.pieces.size == 0) {
				_blocks.removeValue(block, true);
			}
		}
	}

	/**
	 * updates all blocks
	 */
	public void updateBlocks() {
		// unglue all pieces into individual blocks
		Array<Block> blocks = new Array<Block>();
		for (Block block : _blocks) {
			for (BlockPiece piece : block.pieces) {
				Block newBlock = new Block();
				newBlock.pieces.add(piece);
				blocks.add(newBlock);
			}
		}
		_blocks.clear();
		_blocks.addAll(blocks);

		// update each block in new collection
		for (Block block : _blocks) {
			updateBlocks(block);
		}
		removeEmptyBlocks();
	}

	/**
	 * 
	 * @param row
	 * @param col
	 */
	public void updateBlocks(int row, int col) {
		Block block = getBlock(row, col);
		if (block != null) {
			updateBlocks(block);
		}
	}

	/**
	 * all blocks around block are glued to it
	 * 
	 * @param block
	 */
	public void updateBlocks(Block block) {
		Array<Block> neighbours = new Array<Block>();

		for (BlockPiece piece : block.pieces) {
			for (int i = 0; i < 4; ++i) {
				int j = i + 2;
				int rowAdd = (i & 1) - ((i & 2) >> 1);
				int colAdd = (j & 1) - ((j & 2) >> 1);

				int row = piece.row + rowAdd;
				int col = piece.col + colAdd;

				Block neighbour = getBlock(row, col);
				if (neighbour != null && neighbour != block
						&& !neighbours.contains(neighbour, true)) {
					neighbours.add(neighbour);
				}
			}
		}

		for (Block neighbour : neighbours) {
			block.pieces.addAll(neighbour.pieces);
			neighbour.pieces.clear();
			_blocks.removeValue(neighbour, true);
		}
	}

	public void addBlock(Block block) {
		_blocks.add(block);
	}
	
	public Array<Block> getBlocks(){
		return _blocks;
	}
}
