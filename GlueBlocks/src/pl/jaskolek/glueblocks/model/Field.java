package pl.jaskolek.glueblocks.model;

public class Field {

	public int row;
	public int col;
	public enum Type{
		NORMAL_GROUND, WALL, OUTSIDE, END_GROUND
	}
	public Type type = Type.NORMAL_GROUND;
	

	public Field(int row, int col) {
		super();
		this.row = row;
		this.col = col;
	}
}
