package pl.jaskolek.glueblocks.model;

import com.badlogic.gdx.utils.Array;

public class Context {

	public int boardsCount = 15;
	public Array<BoardData> boardStates = new Array<BoardData>();

	public Board currentBoard = null;
	
	
	public Context() {
		super();
		
		for( int i = 0; i < boardsCount; ++i ){
			boardStates.add( new BoardData(i+1) );
		}
	}
	
	
	
}
